﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawMapBehavior : MonoBehaviour
{
   
    public static void DrawTileColor(List<GameObject> gameObjects, Material material,CharacterInfo.UnitType unitType,MapManager mapManager)
    {
        foreach (var tile in gameObjects)
        {
            var utility = tile.GetComponent<RayCastUtility>();
            utility.UpdateRaycast();
            var hitTile = utility.hitObject;

            if(hitTile==null)
            {
                continue;
            }

            var mapInfo = mapManager.FindMapInfo(hitTile);
            if( mapInfo!=null)
            {
                mapInfo.unitType = unitType;
            }

            var render = hitTile.GetComponent<MeshRenderer>();

            if(render==null)
            {
                continue;
            }

            render.material = material;
        }
    }
}