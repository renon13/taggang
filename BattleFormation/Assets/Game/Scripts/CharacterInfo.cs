﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// キャラクター情報クラス
/// </summary>
public class CharacterInfo : MonoBehaviour
{

    [SerializeField] private int money = 0;
    public int Money { get => money; }

    [SerializeField]
    public Tier tier;

    [SerializeField]
    public DrawType[] drawTypes;

    public UnitType unitType { get;set; }
    public enum UnitType
    {
        Player,
        Enemy,
        None,
    }

    public enum Tier
    {
        Tier1=2,
        Tier2,
        Tier3,
        Tier4,
        Tier5,
        Tier6,
        Tier7,
    }

    public enum DrawType
    {
        UP,
        RIGHT,
        LEFT,
        DOWN,
    }

    [SerializeField] private GameObject tile = null;
    private const float mapWidth = 10f;

    ActionBehaviour actionBehaviour;
    private List<GameObject> previewTiles = new List<GameObject>();

    private List<Vector3> tileOffSets = new List<Vector3>();

    private void Start()
    {
        actionBehaviour = FindObjectOfType<ActionBehaviour>();
    }

    public void DestroyTileMap()
    {
        if (previewTiles != null)
        {
            for (int i = 0; i < previewTiles.Count; i++)
            {
                Destroy(previewTiles[i].gameObject);
            }

            previewTiles.Clear();
        }
    }

    public List<GameObject> GetPreviewTiles()
    {
        return previewTiles;
    }

    public void CreateTileMap()
    {
        for (int i = 0; i < (int)tier*drawTypes.Length; i++)
        {
            previewTiles.Add(Instantiate(tile, Vector3.zero, Quaternion.identity));
        }
    }


    private Vector3 GettileoffSet(DrawType drawType)
    {
        float value = 0;
        if (unitType == UnitType.Player)
        {
            value=mapWidth;
        }
        else if (unitType == UnitType.Enemy)
        {
            value=-mapWidth;
        }

        switch (drawType)
        {
            case DrawType.UP:
                return new Vector3(0, 0, value);
            case DrawType.RIGHT:
                return new Vector3(value, 0, 0);
            case DrawType.LEFT:
                return new Vector3(-value, 0, 0);
            case DrawType.DOWN:
                return new Vector3(0, 0, -value);
            default:
                return Vector3.zero;
        }

    }

    private void Update()
    {
        if(previewTiles!=null&&previewTiles.Count>0)
        {
            int tileIndex = 0;
            int drawIndex = 1;
            foreach (var item in drawTypes)
            {
                Vector3 value = Vector3.zero;
                var offSet=GettileoffSet(item);
                for (; tileIndex < (int)tier* drawIndex; tileIndex++)
                {
                    previewTiles[tileIndex].transform.position = actionBehaviour.HitPosition() + value;
                    value += offSet;
                }
                drawIndex++;
            }
        }
    }
}