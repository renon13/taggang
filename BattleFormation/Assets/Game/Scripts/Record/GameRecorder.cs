﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class GameRecorder : MonoBehaviour
{


    List<RecordLogData> recordLogDatas = new List<RecordLogData>();


    public void Record(RecordLogData recordLogData)
    {

        recordLogDatas.Add(recordLogData);

    }


    public void OutPutLogData()
    {
        StreamWriter streamWriter;
        FileInfo fileInfo;
        fileInfo = new FileInfo(Application.dataPath + "/Resources/" + "data01.csv");
        streamWriter = fileInfo.CreateText();
        for (int i = 0; i < recordLogDatas.Count; i++)
        {
            streamWriter.WriteLine(recordLogDatas[i].ToString());
        }
        streamWriter.Flush();
        streamWriter.Close();
    }

}
