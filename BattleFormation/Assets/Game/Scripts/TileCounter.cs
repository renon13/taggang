﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TileCounter : MonoBehaviour
{
    [SerializeField] private CharacterInfo.UnitType unitType = CharacterInfo.UnitType.None;
    [SerializeField] private Text text = null;

    private MapManager mapManager;

    private void Start()
    {
        mapManager = FindObjectOfType<MapManager>();
    }

    // Update is called once per frame
    void Update()
    {
        text.text = mapManager.GetMapInfoCountToUnitType(unitType).ToString();
    }
}
