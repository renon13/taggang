﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCastUtility : MonoBehaviour
{
    private RaycastHit raycastHit = new RaycastHit();
    private bool isRayhit = false;
    [SerializeField]private MeshRenderer meshRenderer = null;

    public GameObject hitObject
    {
        get
        {
            if(raycastHit.collider==null)
            {
    
                return null;
            }

            return raycastHit.collider.gameObject;
        }
    }

    public void UpdateRaycast()
    {
        Ray ray = new Ray(transform.position+new Vector3(0,10,0), Vector3.down);
        isRayhit = Physics.Raycast(ray, out raycastHit);
    }


    private void CheckMeshRender()
    {
        meshRenderer.enabled = hitObject != null;
    }


    private void Update()
    {
        UpdateRaycast();
        CheckMeshRender();
    }
}
