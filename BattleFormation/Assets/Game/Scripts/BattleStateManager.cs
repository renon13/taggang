﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleStateManager : MonoBehaviour
{

    public delegate void OnChangeBattleState(BattleState battleState);
    public static OnChangeBattleState OnChangeBattleStateEvent = (state) => { };

    [SerializeField] private GameObject _playerTurnTextObject = null;
    [SerializeField] private GameObject _enemyTurnTextObject = null;

    /// <summary>
    /// バトル状態の種別
    /// </summary>
    public enum BattleState
    {
        PlayerAction,
        EnemyAction,
        GameEnd,
    }

    /// <summary>
    /// バトル状態
    /// </summary>
    public static BattleState _gameBattleState = BattleState.PlayerAction;
    public BattleState _prevGameBattleState = BattleState.PlayerAction;

    // Start is called before the first frame update
    void Start()
    {
        _gameBattleState = BattleState.PlayerAction;
        _prevGameBattleState = BattleState.EnemyAction;
        ChangeBattleState(_gameBattleState);
    }

    /// <summary>
    /// バトル状態の変更
    /// </summary>
    public static void ChangeBattleState(BattleState battleState)
    {
        _gameBattleState = battleState;
        OnChangeBattleStateEvent(battleState);
    }


    public void ToggleBattleState()
    {
        _playerTurnTextObject.SetActive(false);
        _enemyTurnTextObject.SetActive(false);
        switch (_gameBattleState)
        {
            case BattleState.PlayerAction:
                ChangeBattleState(BattleState.EnemyAction);
                break;
            case BattleState.EnemyAction:
                ChangeBattleState(BattleState.PlayerAction);
                break;
            default:
                break;
        }
    }

    private void Update()
    {
        if(_prevGameBattleState!=_gameBattleState)
        {
            switch (_gameBattleState)
            {
                case BattleState.PlayerAction:
                    StartCoroutine(TurnText(_playerTurnTextObject));
                    break;
                case BattleState.EnemyAction:
                    StartCoroutine(TurnText(_enemyTurnTextObject));
                    break;
                case BattleState.GameEnd:
                    break;
                default:
                    break;
            }
            _prevGameBattleState = _gameBattleState;
        }
    }

    IEnumerator TurnText(GameObject gameObject)
    {
        gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        gameObject.SetActive(false);
    }

}