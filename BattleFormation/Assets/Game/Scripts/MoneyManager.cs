﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyManager : MonoBehaviour
{

    [SerializeField] private MoneyInfo _playerMoney = null;
    [SerializeField] private MoneyInfo _enemyMoney = null;

    private int _playerBonusMoney=0;
    private int _enemyBonusMoney = 0;

    private MapManager mapManager;

    // Start is called before the first frame update
    void Awake()
    {
        BattleStateManager.OnChangeBattleStateEvent += AddMoney;

        var purchaseManager = FindObjectOfType<PurchaseManager>();
        purchaseManager.OnPurchaseEvent += SubMoney;
        purchaseManager.OnPurchaseCheckEvent += PurchaseCheck;

        mapManager = FindObjectOfType<MapManager>();
    }

    private int CalcBonusMoney(CharacterInfo.UnitType unitType)
    {
        int bonusMoney = 0;

        for (MapManager.HorizontalLine i = 0; i < MapManager.HorizontalLine.MAX; i++)
        {
            if(mapManager.IsHorizontalSameUnitType(unitType, i))
            {
                bonusMoney++;
            }
        }

        for (MapManager.VerticalLine i = 0; i < MapManager.VerticalLine.MAX; i++)
        {
            if (mapManager.IsVerticalSameUnitType(unitType, i))
            {
                bonusMoney++;
            }
        }

        return bonusMoney;
    }

    private void AddMoney(BattleStateManager.BattleState battleState)
    {
        switch (battleState)
        {
            case BattleStateManager.BattleState.PlayerAction:
                _playerMoney.AddMoney(1 + CalcBonusMoney(CharacterInfo.UnitType.Player));
                break;
            case BattleStateManager.BattleState.EnemyAction:
                _enemyMoney.AddMoney(1 + CalcBonusMoney(CharacterInfo.UnitType.Enemy));
                break;
        }
    }


    private void SubMoney(CharacterInfo characterInfo)
    {
        switch (BattleStateManager._gameBattleState)
        {
            case BattleStateManager.BattleState.PlayerAction:
                _playerMoney.SubMoney(characterInfo.Money);
                break;
            case BattleStateManager.BattleState.EnemyAction:
                _enemyMoney.SubMoney(characterInfo.Money);
                break;
            case BattleStateManager.BattleState.GameEnd:
                break;
            default:
                break;
        }
    }


    private bool PurchaseCheck(CharacterInfo characterInfo)
    {
        switch (BattleStateManager._gameBattleState)
        {
            case BattleStateManager.BattleState.PlayerAction:
                if (_playerMoney._money >= characterInfo.Money)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            case BattleStateManager.BattleState.EnemyAction:
                if (_enemyMoney._money >= characterInfo.Money)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            case BattleStateManager.BattleState.GameEnd:
                return false;
            default:
                return false;
        }
    }


}
