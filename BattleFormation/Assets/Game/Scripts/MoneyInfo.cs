﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyInfo : MonoBehaviour
{
    [SerializeField] private Text _moneyText = null;

    public int _money=0;

    /// <summary>
    /// お金を増やす
    /// </summary>
    public void AddMoney(int value)
    {
        _money += value;
    }

    /// <summary>
    /// お金を減らす
    /// </summary>
    public void SubMoney(int value)
    {
        _money -= value;
        if(_money<=0)
        {
            _money = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        _moneyText.text = _money.ToString();
    }
}
