﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapInfo
{
    public MapInfo(GameObject fieldTipObject)
    {
        this.fieldTipObject = fieldTipObject;
    }

    public enum MapTileColor
    {
        NoneColor,
        BlueColor,
        RedColor,
    }

    public GameObject fieldTipObject = null;
    public MapTileColor mapTileColor = MapTileColor.NoneColor;
    public CharacterInfo characterInfo = null;
    public CharacterInfo.UnitType unitType = CharacterInfo.UnitType.None;

    public void Register(
        CharacterInfo characterInfo,
        GameObject filedTipObject)
    {
        this.characterInfo = characterInfo;


        // 座標設定
        Transform transform = characterInfo.transform;
        transform.position = fieldTipObject.transform.position;
        transform.rotation = Quaternion.identity;

        switch (characterInfo.unitType)
        {
            case CharacterInfo.UnitType.Player:
                transform.Rotate(transform.up,0,Space.Self);
                break;
            case CharacterInfo.UnitType.Enemy:
                transform.Rotate(transform.up, 180, Space.Self);
                break;
            default:
                break;
        }
    }

    public CharacterInfo.UnitType GetUnitType()
    {
        return unitType;
    }
}
