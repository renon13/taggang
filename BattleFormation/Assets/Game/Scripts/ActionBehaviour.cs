﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionBehaviour : MonoBehaviour
{
    [SerializeField]
    private Material hitMaterial;
    [SerializeField]
    private Renderer rayhitRenderer;
    [SerializeField]
    LayerMask layerMask = new LayerMask();
    private RaycastHit raycastHit = new RaycastHit();
    private bool isRayhit = false;
    private PurchaseManager purchaseManager = null;
    CharacterInfo charInfo;

    private void Start()
    {
        purchaseManager = FindObjectOfType<PurchaseManager>();
    }

    // Update is called once per frame
    private void Update()
    {

        if(BattleStateManager._gameBattleState!=BattleStateManager.BattleState.GameEnd)
        {
            Purchase();
            UpdateRaycast();
        }

    }

    /// <summary>
    /// レイキャスト
    /// </summary>
    private void UpdateRaycast()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        isRayhit = Physics.Raycast(ray, out raycastHit, 10000,layerMask);
    }

    public Vector3 HitPosition()
    {
        if(raycastHit.collider==null)
        {
            return Vector3.zero;
        }

        if(LayerMask.LayerToName(raycastHit.collider.gameObject.layer)!="Tile")
        {
            return Vector3.zero;
        }

        return raycastHit.collider.transform.position;
    }

    /// <summary>
    /// キャラクターの移動
    /// </summary>
    private void Move()
    {

    }

    /// <summary>
    /// キャラクターの攻撃
    /// </summary>
    private void Attack()
    {

    }

    /// <summary>
    /// キャラクターを購入する
    /// </summary>
    private void Purchase()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (isRayhit)
            {
                CharacterInfo info = raycastHit.collider.GetComponent<CharacterInfo>();
                // 予約（キャラをつかむだけ）
                charInfo = purchaseManager.Reservation(info);

                if (charInfo == null)
                {
                    return;
                }

                switch (BattleStateManager._gameBattleState)
                {
                    case BattleStateManager.BattleState.PlayerAction:
                        charInfo.unitType = CharacterInfo.UnitType.Player;
                        break;
                    case BattleStateManager.BattleState.EnemyAction:
                        charInfo.unitType = CharacterInfo.UnitType.Enemy;
                        break;
                    case BattleStateManager.BattleState.GameEnd:
                        break;
                    default:
                        break;
                }

                charInfo.CreateTileMap();
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (raycastHit.collider)
            {
                // 購入成功
                purchaseManager.Purchase(raycastHit.collider.gameObject);
            }
            else
            {
                // 購入失敗
                purchaseManager.Purchase(null);
            }

            if(charInfo!=null)
            {
                charInfo.DestroyTileMap();
            }

        }
    }
}