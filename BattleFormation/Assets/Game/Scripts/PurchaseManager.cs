﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurchaseManager : MonoBehaviour
{

    public delegate void OnPurchase(CharacterInfo characterInfo);
    public OnPurchase OnPurchaseEvent = (characterInfo) => { };

    public delegate bool OnPurchaseCheck(CharacterInfo characterInfo);
    public OnPurchaseCheck OnPurchaseCheckEvent = (characterInfo) => { return false; };

    public CharacterInfo Reservation(CharacterInfo characterInfo)
    {
        if (characterInfo == null)
        {
            return null;
        }

        CharacterInfo findInfo = null;

        foreach (CharacterInfo info in characterInfos)
        {
            if (characterInfo.gameObject == info.gameObject)
            {
                findInfo = info;
                break;
            }
        }

        if (findInfo == null)
        {
            return null;
        }
        else
        {
            if(!OnPurchaseCheckEvent(findInfo))
            {
                return null;
            }
        }

        currentCharacterInfo =
                Instantiate(findInfo,
                characterInfo.transform.position,
                characterInfo.transform.rotation);

        Destroy(currentCharacterInfo.GetComponent<Collider>());

        characterDistance = new Vector3(0, characterInfo.transform.position.y, 0);

        return currentCharacterInfo;
    }

    public bool Purchase(GameObject mapTileObject)
    {
        if (currentCharacterInfo == null)
        {
            return false;
        }

        // キャラ情報の確定
        switch (BattleStateManager._gameBattleState)
        {
            case BattleStateManager.BattleState.PlayerAction:
                currentCharacterInfo.unitType = CharacterInfo.UnitType.Player;
                break;
            case BattleStateManager.BattleState.EnemyAction:
                currentCharacterInfo.unitType = CharacterInfo.UnitType.Enemy;
                break;
            case BattleStateManager.BattleState.GameEnd:
                break;
            default:
                break;
        }

        // マップにキャラを登録する
        bool result = mapManager.RegisterMapInfo(currentCharacterInfo, mapTileObject);
        if (result)
        {
            OnPurchaseEvent(currentCharacterInfo);
            currentCharacterInfo = null;


            switch (BattleStateManager._gameBattleState)
            {
                case BattleStateManager.BattleState.PlayerAction:
                    BattleStateManager.ChangeBattleState(BattleStateManager.BattleState.EnemyAction);
                    break;
                case BattleStateManager.BattleState.EnemyAction:
                    BattleStateManager.ChangeBattleState(BattleStateManager.BattleState.PlayerAction);
                    break;
                case BattleStateManager.BattleState.GameEnd:
                    break;
                default:
                    break;
            }
            return true;
        }
        else
        {
            Destroy(currentCharacterInfo.gameObject);
            return false;
        }
    }

    [SerializeField]
    private CharacterInfo[] characterInfos;

    private CharacterInfo currentCharacterInfo = null;
    private MapManager mapManager;
    private Vector3 characterDistance;

    private void Start()
    {
        mapManager = FindObjectOfType<MapManager>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (currentCharacterInfo == null)
        {
            return;
        }

        MovePoisition();
    }

    private void MovePoisition()
    {
        Vector3 objectPointInScreen
                  = Camera.main.WorldToScreenPoint(currentCharacterInfo.transform.position);

        Vector3 mousePointInScreen
            = new Vector3(Input.mousePosition.x,
                          Input.mousePosition.y,
                          objectPointInScreen.z);

        Vector3 mousePointInWorld = Camera.main.ScreenToWorldPoint(mousePointInScreen);
        //mousePointInWorld.y = currentCharacterInfo.transform.position.y;
        currentCharacterInfo.transform.position = mousePointInWorld;
    }
}
