﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class RecordLogPlayer : MonoBehaviour
{
    List<RecordLogData> recordLogDatas = new List<RecordLogData>();



    private void ConvertToRecordLogData()
    {
        TextAsset csvFile;

        csvFile = Resources.Load("data01") as TextAsset; // Resouces下のCSV読み込み


        StringReader reader = new StringReader(csvFile.text);

        // , で分割しつつ一行ずつ読み込み
        // リストに追加していく
        while (reader.Peek() != -1) // reader.Peaekが-1になるまで
        {
            string line = reader.ReadLine(); // 一行ずつ読み込み

            RecordLogData data = new RecordLogData();
            int index  = line.IndexOf(")");
            string vector3Text = line.Substring(0,index);


            recordLogDatas.Add(data);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {

        ConvertToRecordLogData();
        }
    }
}

[System.Serializable]
public class RecordLogData:ScriptableObject
{
    Vector3 vector3 = Vector3.zero;
    enum Direction
    {
        up,
        down,
    }
    //Direction direction = Direction.up;

    //CharacterInfo.UnitType unitType = CharacterInfo.UnitType.Player;
    //string charaName = string.Empty;

    public RecordLogData()
    {

    }

    public RecordLogData(Vector3 vector3)//Direction direction, CharacterInfo.UnitType unitType, string charaName)
    {
        this.vector3 = vector3;
       // this.direction = direction;
       // this.unitType = unitType;
       // this.charaName = charaName;
    }

    public override string ToString()
    {
        return vector3.ToString();
    }
}
