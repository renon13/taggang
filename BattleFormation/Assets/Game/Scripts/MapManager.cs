﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    [SerializeField] private Material _playerColor = null;
    [SerializeField] private Material _enemyColor = null;

    [SerializeField] private GameObject playerWinText = null;
    [SerializeField] private GameObject enemyWinText = null;
    [SerializeField] private GameObject drawWinText = null;

    [SerializeField] private GameRecorder gameRecorder = null;

    private MapInfo[] mapInfos;

    public enum VerticalLine
    {
        A,
        B,
        C,
        D,
        E,
        F,
        G,
        H,
        MAX,
    }

    public enum HorizontalLine
    {
        ONE,
        TWO,
        THREE,
        FOUR,
        FIVE,
        SIX,
        SEVEN,
        EIGHT,
        MAX,
    }



    /// <summary>
    /// マップにキャラを登録する
    /// </summary>
    /// <param name="characterInfo"></param>
    /// <param name="mapTileObject"></param>
    /// <returns></returns>
    public bool RegisterMapInfo(CharacterInfo characterInfo, GameObject mapTileObject)
    {
        var info = FindMapInfo(mapTileObject);

        if (info != null)
        {
            if (info.unitType != CharacterInfo.UnitType.None)
            {
                return false;
            }

            Material material =null;
            switch (characterInfo.unitType)
            {
                case CharacterInfo.UnitType.Player:
                    material = _playerColor;
                    break;
                case CharacterInfo.UnitType.Enemy:
                    material = _enemyColor;
                    break;
                default:
                    break;
            }
            info.Register(
                       characterInfo,
                       info.fieldTipObject);


            RecordLogData recordLogData = new RecordLogData(characterInfo.transform.position);
            gameRecorder.Record(recordLogData);

            DrawMapBehavior.DrawTileColor(characterInfo.GetPreviewTiles(), material, characterInfo.unitType,this);

            if(CheckGameEnd())
            {
                //ゲーム終了
                BattleStateManager.ChangeBattleState(BattleStateManager.BattleState.GameEnd);
                if(GetMapInfoCountToUnitType(CharacterInfo.UnitType.Player)== GetMapInfoCountToUnitType(CharacterInfo.UnitType.Enemy))
                {
                    drawWinText.SetActive(true);
                }
                else if(GetMapInfoCountToUnitType(CharacterInfo.UnitType.Player)> GetMapInfoCountToUnitType(CharacterInfo.UnitType.Enemy))
                {
                    playerWinText.SetActive(true);
                }
                else
                {
                    enemyWinText.SetActive(true);
                }

                gameRecorder.OutPutLogData();

             //   StartCoroutine(Restart());

            }

            return true;
        
        }

        return false;
    }


    IEnumerator Restart()
    {
        yield return new WaitForSeconds(3);
        UnityEngine.SceneManagement.SceneManager.LoadScene("InGame");
    }


    public int GetMapInfoCountToUnitType(CharacterInfo.UnitType unitType)
    {
        int count = 0;

        foreach (var item in mapInfos)
        {
            if(item.GetUnitType()==unitType)
            {
                count++;
            }
        }
        return count;
    }

    public MapInfo FindMapInfo(GameObject gameObject)
    {
        foreach (var item in mapInfos)
        {
            if(item.fieldTipObject==gameObject)
            {
                return item;
            }
        }
        return null;
    }


    private bool CheckGameEnd()
    {
        foreach (var item in mapInfos)
        {
            if (item.unitType == CharacterInfo.UnitType.None)
            {
                return false;
            }
        }
        return true;
    }

    // Start is called before the first frame update
    private void Start()
    {
        Transform[] childrenTransform = gameObject.GetComponentsInChildren<Transform>();
        GameObject[] children = childrenTransform.ToList().Select(a => a.gameObject).ToArray();

        List<MapInfo> mapInfoList = new List<MapInfo>();
        foreach (GameObject child in children)
        {
            mapInfoList.Add(new MapInfo(child));
        }
        mapInfoList.RemoveAt(0);
        mapInfos = mapInfoList.ToArray();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.A))
        {
            gameRecorder.OutPutLogData();

        }
    }


    public bool IsHorizontalSameUnitType(CharacterInfo.UnitType unitType,HorizontalLine horizontalLine)
    {
        for (int i = (int)horizontalLine; i < mapInfos.Length; i+= (int)HorizontalLine.MAX)
        {
            var info = mapInfos[i];
            if(info.unitType!=unitType)
            {
                return false;
            }
        }
        return true;
    }

    public bool IsVerticalSameUnitType(CharacterInfo.UnitType unitType, VerticalLine verticalLine)
    {
        for (int i = (int)verticalLine; i < mapInfos.Length; i += (int)VerticalLine.MAX)
        {
            var info = mapInfos[i];
            if (info.unitType != unitType)
            {
                return false;
            }
        }
        return true;
    }
}
